#=====================================================================================================
# Pyramidal Signed Distance Learning for Spatio-Temporal Human Shape Completion
# Copyright Inria
# Year 2022
# This file is released under the agreement of License.pdf from the git repository.
##
# This repository has code derived from:
## (1) 2020 Julian Chibane, Max-Planck-Gesellschaft (License: https://github.com/jchibane/if-net)
## (2) 2019 Lars Mescheder, Michael Oechsle, Michael Niemeyer, Andreas Geiger, 
## Sebastian Nowozin (MIT License: https://github.com/autonomousvision/occupancy_networks)
## (3) U-Net MICCAI 2015 ( GNU General Public License v3.0: https://github.com/milesial/Pytorch-UNet)
#=====================================================================================================

import torch 
import trimesh
import torch.nn.functional as F
import torch.nn as nn
import numpy as np
from torch import optim
import glob
from torch.utils.data import DataLoader, Dataset

def todigits(num, len_num):
    num = str(num)
    count_num = len(num)
    if len_num == 6:
        if count_num == 1:
            return '00000'+num 
        elif count_num == 2:
            return '0000'+num
        elif count_num == 3:
            return '000'+num
        elif count_num == 4:
            return '00'+num
        elif count_num == 5:
            return '0'+num
        else:
            return num

    elif len_num == 5:
        if count_num == 1:
            return '0000'+num 
        elif count_num == 2:
            return '000'+num
        elif count_num == 3:
            return '00'+num
        elif count_num == 4:
            return '0'+num
        else:
            return num

    elif len_num == 4:
        if count_num == 1:
            return '000'+num 
        elif count_num == 2:
            return '00'+num
        elif count_num == 3:
            return '0'+num
        else:
            return num

    else:
        print('wrongggggg')

class DoubleConv(nn.Module):
    """(convolution => ReLU) * 2"""

    def __init__(self, in_channels, out_channels, mid_channels=None):
        super(DoubleConv, self).__init__()
        if not mid_channels:
            mid_channels = out_channels
        self.double_conv = nn.Sequential(
            nn.Conv2d(in_channels, mid_channels, kernel_size=3, padding=1),
            nn.ReLU(inplace=True),
            nn.Conv2d(mid_channels, out_channels, kernel_size=3, padding=1),
            nn.ReLU(inplace=True)
        )

    def forward(self, x):
        return self.double_conv(x)

class Down(nn.Module):
    """Downscaling with maxpool then double conv"""

    def __init__(self, in_channels, out_channels):
        super().__init__()
        self.maxpool_conv = nn.Sequential(
            nn.MaxPool2d(2),
            DoubleConv(in_channels, out_channels)
        )

    def forward(self, x):
        return self.maxpool_conv(x)

class Up(nn.Module):
    """Upscaling then double conv"""

    def __init__(self, in_channels, out_channels, bilinear=True):
        super().__init__()

        # if bilinear, use the normal convolutions to reduce the number of channels
        if bilinear:
            self.up = nn.Upsample(scale_factor=2, mode='bilinear', align_corners=False)
            self.conv = DoubleConv(in_channels, out_channels, in_channels // 2)
        else:
            self.up = nn.ConvTranspose2d(in_channels , in_channels // 2, kernel_size=2, stride=2)
            self.conv = DoubleConv(in_channels, out_channels)


    def forward(self, x1, x2):
        x1 = self.up(x1)
        # input is CHW
        diffY = x2.size()[2] - x1.size()[2]
        diffX = x2.size()[3] - x1.size()[3]

        x1 = F.pad(x1, [diffX // 2, diffX - diffX // 2,
                        diffY // 2, diffY - diffY // 2])

        x = torch.cat([x2, x1], dim=1)
        return self.conv(x)

class UGRUNet_hie(nn.Module):
    def __init__(self, n_channels, out_channels, batss, bilinear=True, device = 'cuda', middle_kernel = 32):
        super(UGRUNet_hie, self).__init__()
        self.device = device 
        self.n_channels = n_channels
        self.out_channels = out_channels
        self.bilinear = bilinear
        self.batss = batss

        self.inc = DoubleConv(n_channels, 32)
        self.down1 = Down(32, 64)
        self.down2 = Down(64, 128)
        self.down3 = Down(128, 256)
        factor = 2 if bilinear else 1
        self.down4 = Down(256, 512 // factor)

        self.convglob = nn.Conv2d(512//factor,256, kernel_size = middle_kernel)
        self.gru = nn.GRU(256, 128, bidirectional = True)
        self.h0 = torch.zeros(2, self.batss, 128).to(self.device)
        self.deconvglob = nn.ConvTranspose2d(256,512//factor, kernel_size = middle_kernel)

        self.up1 = Up(512, 256 // factor, bilinear)
        self.up2 = Up(256, 128 // factor, bilinear)
        self.up3 = Up(128, 64 // factor, bilinear)
        self.up4 = Up(64, 64, bilinear)
        self.outc = OutConv(64, out_channels)
        self.outc_down1 = OutConv(64 // factor, out_channels)
        self.outc_down2 = OutConv(128 // factor, out_channels)

    def forward(self, x):
        B, C, dep_n, res1_, res2_ = x.size()
        x11 = self.inc(x[:,0,:,:,:])
        x12 = self.down1(x11)
        x13 = self.down2(x12)
        x14 = self.down3(x13)
        x15 = self.down4(x14)
        x1g = self.convglob(x15).view(B, 1, 256)

        x21 = self.inc(x[:,1,:,:,:])
        x22 = self.down1(x21)
        x23 = self.down2(x22)
        x24 = self.down3(x23)
        x25 = self.down4(x24)
        x2g = self.convglob(x25).view(B, 1, 256)

        x31 = self.inc(x[:,2,:,:,:])
        x32 = self.down1(x31)
        x33 = self.down2(x32)
        x34 = self.down3(x33)
        x35 = self.down4(x34)
        x3g = self.convglob(x35).view(B, 1, 256)

        x41 = self.inc(x[:,3,:,:,:])
        x42 = self.down1(x41)
        x43 = self.down2(x42)
        x44 = self.down3(x43)
        x45 = self.down4(x44)
        x4g = self.convglob(x45).view(B, 1, 256)

        x_gru_input = torch.cat([x1g, x2g, x3g, x4g], 1)
        # B, 4, 256
        x_gru_input = x_gru_input.transpose(0,1)
        # 4 seq, B, 256
        x_gru, h_gru = self.gru(x_gru_input, self.h0)
        # 4 seq, B, 256

        x_gru = x_gru.transpose(0,1)
        # B, 4, 256
        x_gru = F.relu(x_gru)

        x15 = x15+self.deconvglob(x_gru[:,0,:].view(B,256,1,1))
        x25 = x25+self.deconvglob(x_gru[:,1,:].view(B,256,1,1))
        x35 = x35+self.deconvglob(x_gru[:,2,:].view(B,256,1,1))
        x45 = x45+self.deconvglob(x_gru[:,3,:].view(B,256,1,1))

        x1 = self.up1(x15, x14)
        x1_down2 = self.up2(x1, x13)
        x1_down1 = self.up3(x1_down2, x12)
        x1 = self.up4(x1_down1, x11)
        logits1 = self.outc(x1).unsqueeze(2)
        logits1_down1 = self.outc_down1(x1_down1).unsqueeze(2)
        logits1_down2 = self.outc_down2(x1_down2).unsqueeze(2)

        x2 = self.up1(x25, x24)
        x2_down2 = self.up2(x2, x23)
        x2_down1 = self.up3(x2_down2, x22)
        x2 = self.up4(x2_down1, x21)
        logits2 = self.outc(x2).unsqueeze(2)
        logits2_down1 = self.outc_down1(x2_down1).unsqueeze(2)
        logits2_down2 = self.outc_down2(x2_down2).unsqueeze(2)

        x3 = self.up1(x35, x34)
        x3_down2 = self.up2(x3, x33)
        x3_down1 = self.up3(x3_down2, x32)
        x3 = self.up4(x3_down1, x31)
        logits3 = self.outc(x3).unsqueeze(2)
        logits3_down1 = self.outc_down1(x3_down1).unsqueeze(2)
        logits3_down2 = self.outc_down2(x3_down2).unsqueeze(2)

        x4 = self.up1(x45, x44)
        x4_down2 = self.up2(x4, x43)
        x4_down1 = self.up3(x4_down2, x42)
        x4 = self.up4(x4_down1, x41)
        logits4 = self.outc(x4).unsqueeze(2)
        logits4_down1 = self.outc_down1(x4_down1).unsqueeze(2)
        logits4_down2 = self.outc_down2(x4_down2).unsqueeze(2)

        logits_origin = torch.cat([logits1, logits2, logits3, logits4], 2)
        logits_down1 = torch.cat([logits1_down1, logits2_down1, logits3_down1, logits4_down1], 2)
        logits_down2 = torch.cat([logits1_down2, logits2_down2, logits3_down2, logits4_down2], 2)
        return [logits_origin, logits_down1, logits_down2]

ROOT = './'#todo


class MyDataset(Dataset):
    def __init__(self, files_p, n_pts, interv_train):
        self.obj_files = files_p
        self.n_pts = n_pts
        self.interv_train = interv_train
        self.nei_num = 4
        self.multi_z = 128.0


    def load_single_data_cubedata(self, base_path, interv_add = 0, no_sampling = 400, ress_origin = 512, surf_index = [], neibor_index_list = [], uni_index = []):
        ROOT_p = ROOT+'data/'
        SUB_SEQ_fl = base_path.split('/')
        SUB = SUB_SEQ_fl[-3]
        SEQ = SUB_SEQ_fl[-2]
        fl_topart = SUB_SEQ_fl[-1]
        fl_parts = fl_topart.split('.')
        fl_nex1 = fl_parts[0]+'.'+todigits(int(fl_parts[1])+interv_add, len(fl_parts[1]))+'.'+fl_parts[2]
        depth_path_nex1_origin = ROOT_p+'/depth_res'+str(ress_origin)+'/'+SUB+'/'+SEQ+'/'+fl_nex1[:-3]+'npz'
        
        if SUB[0] == '5':
            fl_nex_follow = todigits(int(fl_parts[1])+interv_add, len(fl_parts[1]))
        else:
            fl_nex_follow = SEQ+'.'+todigits(int(fl_parts[1])+interv_add, len(fl_parts[1]))

        point_path_nex1_down1 = ROOT_p+'/tsdf_rescale_res'+str(ress_origin//2)+'/'+SUB+'/'+SEQ+'/'+fl_nex_follow+'.npz'
        point_path_nex1_down2 = ROOT_p+'/tsdf_rescale_res'+str(ress_origin//4)+'/'+SUB+'/'+SEQ+'/'+fl_nex_follow+'.npz'
        point_path_nex1_origin = ROOT_p+'/tsdf_rescale_res'+str(ress_origin)+'/'+SUB+'/'+SEQ+'/'+fl_nex_follow+'.npz'

        path_uni = ROOT_p+'/tsdf_more_uniform/'+SUB+'/'+SEQ+'/'+fl_parts[1]+'interv'+str(self.interv_train)
        point_path_nex1_out = path_uni+'/uniformpts_'+fl_parts[1]+'.npz'
        sdf_path_nex1_uni = path_uni+'/uniformsdf_'+todigits(int(fl_parts[1])+interv_add, len(fl_parts[1]))+'.npy'
        ######sdf path and load and query pts z trans

        npps_nex1_origin = np.load(depth_path_nex1_origin)
        cent_val = npps_nex1_origin['cent_v']
        depth_nex1_raw = npps_nex1_origin['depth_cent_img'].reshape((ress_origin, ress_origin))
        
        depth_nex1_origin = depth_nex1_raw.reshape((1, ress_origin, ress_origin))

        npps_surf_nex1_down2 = np.load(point_path_nex1_down2)
        npps_surf_nex1_down1 = np.load(point_path_nex1_down1)
        npps_surf_nex1_origin = np.load(point_path_nex1_origin)

        surf = npps_surf_nex1_origin['surface']
        
        pts_nei_down1 = npps_surf_nex1_down1['surface_displaced']
        pts_nei_down2 = npps_surf_nex1_down2['surface_displaced']
        pts_nei_origin = npps_surf_nex1_origin['surface_displaced']
        sdf_nei_down1 = npps_surf_nex1_down1['sdf_displaced']
        sdf_nei_down2 = npps_surf_nex1_down2['sdf_displaced']
        sdf_nei_origin = npps_surf_nex1_origin['sdf_displaced']

        pts_nei_down1_select = []
        pts_nei_down2_select = []
        pts_nei_origin_select = []
        sdf_nei_down1_select = []
        sdf_nei_down2_select = []
        sdf_nei_origin_select = []

        surface_pts = surf[surf_index]

        for sii, i_d in enumerate(surf_index):
            nei_index = neibor_index_list[sii]
            pts_nei_down1_select.append(pts_nei_down1[i_d][nei_index])
            pts_nei_down2_select.append(pts_nei_down2[i_d][nei_index])
            pts_nei_origin_select.append(pts_nei_origin[i_d][nei_index])

            sdf_nei_down1_select.append(sdf_nei_down1[i_d][nei_index])
            sdf_nei_down2_select.append(sdf_nei_down2[i_d][nei_index])
            sdf_nei_origin_select.append(sdf_nei_origin[i_d][nei_index])


        pts_nei_down1_select = np.array(pts_nei_down1_select).reshape((no_sampling*self.nei_num, 3))
        pts_nei_down2_select = np.array(pts_nei_down2_select).reshape((no_sampling*self.nei_num, 3))
        pts_nei_origin_select = np.array(pts_nei_origin_select).reshape((no_sampling*self.nei_num, 3))
        sdf_nei_down1_select = np.array(sdf_nei_down1_select).reshape((no_sampling*self.nei_num, 1))
        sdf_nei_down2_select = np.array(sdf_nei_down2_select).reshape((no_sampling*self.nei_num, 1)) 
        sdf_nei_origin_select = np.array(sdf_nei_origin_select).reshape((no_sampling*self.nei_num, 1)) 

        out_out_npp = np.load(point_path_nex1_out)

        out_out_pts = out_out_npp['uniform_query_pts']
        
        pts_out_out2 = out_out_pts[uni_index]
        Y_out_uni2 = np.load(sdf_path_nex1_uni)
        Y_out_out2 = Y_out_uni2[uni_index]
        Y_out_out2 = Y_out_out2.reshape((-1, 1))

        ###########centralize z for selected pts#######
        surface_pts[:,2] -= cent_val 
        pts_nei_down1_select[:,2] -= cent_val
        pts_nei_down2_select[:,2] -= cent_val
        pts_nei_origin_select[:,2] -= cent_val
        pts_out_out2[:,2] -= cent_val

        surface_pts[:,2] *= self.multi_z 
        pts_nei_down1_select[:,2] *= self.multi_z
        pts_nei_down2_select[:,2] *= self.multi_z
        pts_nei_origin_select[:,2] *= self.multi_z
        pts_out_out2[:,2] *= self.multi_z


        X_down1 = np.concatenate((surface_pts, pts_nei_down1_select, pts_out_out2), axis = 0) # no_sampling*7, 3
        X_down2 = np.concatenate((surface_pts, pts_nei_down2_select, pts_out_out2), axis = 0) # no_sampling*7, 3
        X_origin = np.concatenate((surface_pts, pts_nei_origin_select, pts_out_out2), axis = 0) # no_sampling*7, 3

        sdf_surface = np.zeros((no_sampling,1))
        Y_down1 = np.concatenate((sdf_surface, sdf_nei_down1_select, Y_out_out2), axis = 0) # no_sampling*7, 1
        Y_down2 = np.concatenate((sdf_surface, sdf_nei_down2_select, Y_out_out2), axis = 0) # no_sampling*7, 1
        Y_origin = np.concatenate((sdf_surface, sdf_nei_origin_select, Y_out_out2), axis = 0) # no_sampling*7, 1
        
        return depth_nex1_origin, X_origin, Y_origin, 0, X_down1, Y_down1, 0, X_down2, Y_down2

    def load_4depth(self, base_path):
        center_rand_choice = np.random.choice(6890, self.n_pts, replace = False)
        neigbor_rand_list = []
        for ci in range(self.n_pts):
            neigbor_rand_list.append(np.random.choice(26, self.nei_num, replace = False))

        unifor_rand_choice = np.random.choice(100000, self.n_pts*2, replace = False)

        dep_cur, X_origin_cur, Y_origin_cur, X_down1_cur, Y_down1_cur, X_down2_cur, Y_down2_cur = self.load_single_data_cubedata(base_path = base_path, no_sampling = self.n_pts, ress_origin = 512, surf_index = center_rand_choice, neibor_index_list = neigbor_rand_list, uni_index = unifor_rand_choice)
        dep_nex1, X_origin_nex1, Y_origin_nex1, X_down1_nex1, Y_down1_nex1, X_down2_nex1, Y_down2_nex1 = self.load_single_data_cubedata(base_path = base_path, interv_add = self.interv_train, no_sampling = self.n_pts, ress_origin = 512, surf_index = center_rand_choice, neibor_index_list = neigbor_rand_list, uni_index = unifor_rand_choice)
        dep_nex2, X_origin_nex2, Y_origin_nex2, X_down1_nex2, Y_down1_nex2, X_down2_nex2, Y_down2_nex2 = self.load_single_data_cubedata(base_path = base_path, interv_add = self.interv_train*2, no_sampling = self.n_pts, ress_origin = 512, surf_index = center_rand_choice, neibor_index_list = neigbor_rand_list, uni_index = unifor_rand_choice)
        dep_nex3, X_origin_nex3, Y_origin_nex3, X_down1_nex3, Y_down1_nex3, X_down2_nex3, Y_down2_nex3 = self.load_single_data_cubedata(base_path = base_path, interv_add = self.interv_train*3, no_sampling = self.n_pts, ress_origin = 512, surf_index = center_rand_choice, neibor_index_list = neigbor_rand_list, uni_index = unifor_rand_choice)

        idx_origin = np.random.permutation(len(X_origin_cur))
        dep_out = np.array([dep_cur, dep_nex1, dep_nex2, dep_nex3])
        X_origin_out = np.array([X_origin_cur[idx_origin], X_origin_nex1[idx_origin], X_origin_nex2[idx_origin], X_origin_nex3[idx_origin]])
        Y_origin_out = np.array([Y_origin_cur[idx_origin], Y_origin_nex1[idx_origin], Y_origin_nex2[idx_origin], Y_origin_nex3[idx_origin]])
        nonon_out1 = 0

        idx_down1 = np.random.permutation(len(X_down1_cur))
        X_down1_out = np.array([X_down1_cur[idx_down1], X_down1_nex1[idx_down1], X_down1_nex2[idx_down1], X_down1_nex3[idx_down1]])
        Y_down1_out = np.array([Y_down1_cur[idx_down1], Y_down1_nex1[idx_down1], Y_down1_nex2[idx_down1], Y_down1_nex3[idx_down1]])
        nono_down2 = 0

        idx_down2 = np.random.permutation(len(X_down2_cur))
        X_down2_out = np.array([X_down2_cur[idx_down2], X_down2_nex1[idx_down2], X_down2_nex2[idx_down2], X_down2_nex3[idx_down2]])
        Y_down2_out = np.array([Y_down2_cur[idx_down2], Y_down2_nex1[idx_down2], Y_down2_nex2[idx_down2], Y_down2_nex3[idx_down2]])
        #print(len(idx_down2), len(idx_down1), len(idx_origin))
        return dep_out, X_origin_out, Y_origin_out, X_down1_out, Y_down1_out, X_down2_out, Y_down2_out

    def __getitem__(self, idx):
        
        return self.load_4depth(self.obj_files[idx])

    def __len__(self):
        return len(self.obj_files)



upsample_2 = nn.Upsample(scale_factor=2, mode='bilinear', align_corners = False)

class CNN_trans(nn.Module):
    def __init__(self, n_channels, out_channels):
        super(CNN_trans, self).__init__()
        self.n_channels = n_channels
        self.out_channels = out_channels

        self.conv1 = nn.Conv3d(self.n_channels, self.out_channels, kernel_size=3, padding=1)

        self.conv2 = nn.Conv3d(self.out_channels, self.out_channels, kernel_size=3, padding=1)

        self.conv3 = nn.Conv3d(self.out_channels, self.out_channels, kernel_size=3, padding=1)

    def forward(self, x, query_pts, feat_init = 0, flag = False):
        B, dep_num, N, _ = query_pts.size()
        
        if flag:
            feat_init1 = upsample_2(feat_init[:,:,0,:,:]).unsqueeze(2)
            feat_init2 = upsample_2(feat_init[:,:,1,:,:]).unsqueeze(2)
            feat_init3 = upsample_2(feat_init[:,:,2,:,:]).unsqueeze(2)
            feat_init4 = upsample_2(feat_init[:,:,3,:,:]).unsqueeze(2)
            feat_init_up = torch.cat((feat_init1, feat_init2, feat_init3, feat_init4), 2)
            x = torch.cat((x, feat_init_up), 1)
            #print('innn', feat_init.size())
        else:
            feat_init_up = 0

        x = F.relu(self.conv1(x))
        x = F.relu(self.conv2(x))
        x = self.conv3(x)+feat_init_up

        feats_out = []
        for i_dep in range(dep_num):
            query_pts_to = query_pts[:,i_dep,:,:].unsqueeze(1)
            #(B,1,N,3)
            
            grid_query = query_pts_to[:,:,:,:2]*(-1.0)

            feats_1 = F.grid_sample(x[:,:,i_dep,:,:], grid_query, mode = 'bilinear', align_corners = False).view(B, self.out_channels, N).transpose(1,2)
            #(B, N, C)
            feats_out.append(feats_1)

        return feats_out, x

def query_feat(x, query_pts):
    B, N, _ = query_pts.size()
    bb_, Cha_s, r_1, r_2 = x.size()
    query_pts = query_pts.unsqueeze(1)
    #(B,1,N,3)
    
    grid_query = query_pts[:,:,:,:2]*(-1.0)

    feats = F.grid_sample(x, grid_query, mode = 'bilinear', align_corners = False).view(B, Cha_s, N).transpose(1,2)
    #(B, N, C)
    return feats


class Decode_PE_IF(nn.Module):
    def __init__(self, in_channels, out_channels):
        super(Decode_PE_IF, self).__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.spa1 = nn.Linear(in_channels, in_channels//2)
        self.spa2 = nn.Linear(in_channels//2, in_channels//2)
        self.spa3 = nn.Linear(in_channels//2, out_channels)

    def forward(self, x):
        outy = F.relu(self.spa1(x))
        outy = F.relu(self.spa2(outy))
        outy = self.spa3(outy)
        return torch.tanh(outy)#voxel output

bottle_neck_dim = 128
BATCH_S = 2
unet_enc = UGRUNet_hie(1, bottle_neck_dim, BATCH_S, bilinear=False).to(device)
conv_trans_init = CNN_trans(bottle_neck_dim, bottle_neck_dim).to(device)
conv_trans = CNN_trans(bottle_neck_dim*2, bottle_neck_dim).to(device)

unet_dec = Decode_PE_IF(bottle_neck_dim+1,1).to(device)
unet_dec256 = Decode_PE_IF(bottle_neck_dim+1,1).to(device)
unet_dec128 = Decode_PE_IF(bottle_neck_dim+1,1).to(device)

files_short = ''#todo
files_long = ''#todo
interv_short = 4
interv_long = 10
dset = MyDataset(files_p = files_short, n_pts= 400, interv_train = interv_short)
dset_long = MyDataset(files_p = files_long, n_pts= 400, interv_train = interv_long)
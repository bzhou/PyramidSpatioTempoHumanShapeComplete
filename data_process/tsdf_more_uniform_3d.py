#=====================================================================================================
# Pyramidal Signed Distance Learning for Spatio-Temporal Human Shape Completion
# Copyright Inria
# Year 2022
# This file is released under the agreement of License.pdf from the git repository.
##
# This repository has code derived from:
## (1) 2020 Julian Chibane, Max-Planck-Gesellschaft (License: https://github.com/jchibane/if-net)
## (2) 2019 Lars Mescheder, Michael Oechsle, Michael Niemeyer, Andreas Geiger, 
## Sebastian Nowozin (MIT License: https://github.com/autonomousvision/occupancy_networks)
## (3) U-Net MICCAI 2015 ( GNU General Public License v3.0: https://github.com/milesial/Pytorch-UNet)
#=====================================================================================================

import torch
from pytorch3d.io import load_obj
from pytorch3d.structures import Meshes
from pytorch3d.renderer import (
    look_at_view_transform,
    FoVPerspectiveCameras,
    PerspectiveCameras, 
    OrthographicCameras,
    PointLights, 
    DirectionalLights, 
    Materials, 
    RasterizationSettings, 
    MeshRenderer, 
    MeshRasterizer,  
    SoftPhongShader,
    TexturesUV,
    TexturesVertex
)
import numpy as np 
import matplotlib.pyplot as plt
import torch.nn as nn 
import trimesh
import argparse
import os
from pytorch3d.renderer.mesh.rasterizer import RasterizationSettings
from pytorch3d.renderer.mesh.rasterizer import MeshRasterizer

import matplotlib.pyplot as plt
import glob
import igl

def create_grid_points_from_bounds(minimun, maximum, res):
    x = np.linspace(minimun, maximum, res)
    X, Y, Z = np.meshgrid(x, x, x, indexing='ij')
    X = X.reshape((np.prod(X.shape),))
    Y = Y.reshape((np.prod(Y.shape),))
    Z = Z.reshape((np.prod(Z.shape),))

    points_list = np.column_stack((X, Y, Z))
    del X, Y, Z, x
    return points_list

def todigits(num, len_num):
    num = str(num)
    count_num = len(num)
    if len_num == 6:
        if count_num == 1:
            return '00000'+num 
        elif count_num == 2:
            return '0000'+num
        elif count_num == 3:
            return '000'+num
        elif count_num == 4:
            return '00'+num
        elif count_num == 5:
            return '0'+num
        else:
            return num

    elif len_num == 5:
        if count_num == 1:
            return '0000'+num 
        elif count_num == 2:
            return '000'+num
        elif count_num == 3:
            return '00'+num
        elif count_num == 4:
            return '0'+num
        else:
            return num

    elif len_num == 4:
        if count_num == 1:
            return '000'+num 
        elif count_num == 2:
            return '00'+num
        elif count_num == 3:
            return '0'+num
        else:
            return num

    else:
        print('wrongggggg')


def compute_xy_dep(sur_points):
    # using pre-define camera
    # compute (ndc_xy, z_depth) 
    sur_points = torch.from_numpy(sur_points).float().to(device).unsqueeze(0)
    sur_xy_ndc = cameras_depth.transform_points_ndc(sur_points)[0, :, :2]
    sur_dep = cameras_depth.get_world_to_view_transform().transform_points(sur_points)[0, :, 2:]
    # extract depth of each point
    sur_xy_dep = torch.cat((sur_xy_ndc, sur_dep), dim = 1)
    sur_xy_dep = sur_xy_dep.detach().cpu().numpy()
    return np.array(sur_xy_dep) 

def uniform_sampling(fl_name):
    if is_CAPE:
        out_folder = out_path+todigits(fl_name, 6)+'interv'+str(INTERV)+'/'
    else:
        out_folder = out_path+todigits(fl_name, 5)+'interv'+str(INTERV)+'/'
    
    os.mkdir(out_folder)
    total_pts_num = 100000
    ##### same unifrom points for all 4 frames ##########
    grid_x = np.random.uniform(bound_box[0,0], bound_box[1,0], total_pts_num).reshape(-1,1)
    grid_y = np.random.uniform(bound_box[0,1], bound_box[1,1], total_pts_num).reshape(-1,1)
    grid_z = np.random.uniform(bound_box[0,2], bound_box[1,2], total_pts_num).reshape(-1,1)
    grid_points = np.column_stack((grid_x, grid_y, grid_z))
    ##### same unifrom points for all 4 frames ##########

    ##### save points ###
    if is_CAPE:
        out_file = out_folder+'uniformpts_'+todigits(fl_name, 6)+'.npz'
    else:
        out_file = out_folder+'uniformpts_'+todigits(fl_name, 5)+'.npz'
    if os.path.exists(out_file):
        print('File exists. Done.')
        return
    
    grid_xy_dep = compute_xy_dep(grid_points)

    np.savez(out_file, uniform_pts = grid_points, uniform_query_pts = grid_xy_dep)
    try:
        set_nega = set(list(range(total_pts_num)))
        set_posi = set(list(range(total_pts_num)))
        for addi in range(4):
            if is_CAPE:
                fl_frame = ROOT +SUB+'/'+SEQ+'/'+SEQ+'.'+todigits((addi*INTERV+fl_name),6)+'.obj'
            else:
                fl_frame = ROOT +SUB+'/'+SUB+'_'+SEQ+'/'+todigits((addi*INTERV+fl_name),5)+'.obj'
            
            gt_mesh = trimesh.load(fl_frame, process=False)
            rescale = 75.0
            #rescale value = 75
            ###### compute signed distance ###########
            sign_dis = -1.*rescale*igl.signed_distance(grid_points, gt_mesh.vertices, gt_mesh.faces)[0]

            sign_dis = np.array(sign_dis)
            reset_nega = np.where(sign_dis<=-1)[0] # reset signed distance lower than threshold to truncated value
            reset_posi = np.where(sign_dis>=1)[0] # reset signed distance larger than threshold to truncated value
            #sur_dis_points = sur_dis_points.reshape((num_sur, 26, 3))
            sign_dis[reset_nega] = -1.
            sign_dis[reset_posi] = 1. 
            ###### compute signed distance ###########
            if is_CAPE:
                sdf_file = out_folder+'uniformsdf_'+todigits((addi*INTERV+fl_name), 6)+'.npy'
            else:
                sdf_file = out_folder+'uniformsdf_'+todigits((addi*INTERV+fl_name), 5)+'.npy'
            if os.path.exists(sdf_file):
                print('File exists. Done.')
                return
            
            np.save(sdf_file, sign_dis)

    except:
        print('error no such file ', out_folder, fl_frame)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='tsdf uniform'
    )
    parser.add_argument('--subj', type=str, help='5-digit subject id')
    parser.add_argument('--seq', type=str, help='sequence name')
    parser.add_argument('--CAPE', type=str, default='CAPE')
    parser.add_argument('--interv', type=int, default=4)

    args = parser.parse_args()
    device = 'cuda'
    SUB = str(args.subj)
    SEQ = str(args.seq)
    is_CAPE = (str(args.CAPE) == 'CAPE')
    INTERV = args.interv

    ROOT = './'#todo
    ##########out_path##########
    out_path = ROOT+'tsdf_uniform/'
    if not os.path.exists(out_path):
        os.mkdir(out_path)
        print('creating path ', out_path)

    out_path = out_path+SUB+'/'
    if not os.path.exists(out_path):
        os.mkdir(out_path)
        print('creating path ', out_path)

    out_path = out_path+SEQ+'/'
    if not os.path.exists(out_path):
        os.mkdir(out_path)
        print('creating path ', out_path)
    else:
        print('path exist')
    ##########out_path##########

    #####define camera####
    fx = 0.8
    fy = 0.8
    px = 0.0
    py = 0.0
    ######### camera instrinsec ########

    ########## camera extrinsec #########
    R_camera = torch.zeros((1,3,3))
    R_camera[0,0,0] = -1.0
    R_camera[0,1,1] = 1.0
    R_camera[0,2,2] = -1.0
    T_camera = torch.tensor([[0.002, -0.481, 2.315]])

    cameras_depth = OrthographicCameras(focal_length = ((fx, fy),), principal_point = ((px, py),), R = R_camera, T=T_camera, device=device)
    #####define camera####
    #here we consider only fx == fy
    assert fx==fy, "focal not same for x and y axis"
    bound_length = 1.0/fx
    #we don't consider px, py and fx = fy
    grid_center = np.array([0.002, 0.481, 2.315-2.5]) #only move z to mean mesh center
    bound_box = np.ones((2,3))

    bound_box[0,:] *= (-bound_length)
    bound_box[1,:] *= bound_length 
    bound_box += grid_center
    ###########grid sample

    

    paths = glob.glob(ROOT +SUB+'/'+SEQ+'/*obj')#todo
    #path to ground truth

    tarlen = int(sorted(paths)[-1].split('/')[-1].split('.')[-2])
    print('---tarlen---', tarlen)
    if INTERV == 4:
        start_fr = 7+9
        end_fr = 120+9
    elif INTERV == 10:
        start_fr = 7
        end_fr = 120
    files = list(range(start_fr,end_fr,22))
    for f_p in files:
        uniform_sampling(f_p)
    print(out_path)

#=====================================================================================================
# Pyramidal Signed Distance Learning for Spatio-Temporal Human Shape Completion
# Copyright Inria
# Year 2022
# This file is released under the agreement of License.pdf from the git repository.
##
# This repository has code derived from:
## (1) 2020 Julian Chibane, Max-Planck-Gesellschaft (License: https://github.com/jchibane/if-net)
## (2) 2019 Lars Mescheder, Michael Oechsle, Michael Niemeyer, Andreas Geiger, 
## Sebastian Nowozin (MIT License: https://github.com/autonomousvision/occupancy_networks)
## (3) U-Net MICCAI 2015 ( GNU General Public License v3.0: https://github.com/milesial/Pytorch-UNet)
#=====================================================================================================


import torch
from pytorch3d.io import load_obj
from pytorch3d.structures import Meshes
from pytorch3d.renderer import (
    look_at_view_transform,
    FoVPerspectiveCameras,
    PerspectiveCameras, 
    OrthographicCameras,
    PointLights, 
    DirectionalLights, 
    Materials, 
    RasterizationSettings, 
    MeshRenderer, 
    MeshRasterizer,  
    SoftPhongShader,
    TexturesUV,
    TexturesVertex
)
import numpy as np 
import matplotlib.pyplot as plt
import torch.nn as nn 
import trimesh
import argparse
import os
from pytorch3d.renderer.mesh.rasterizer import RasterizationSettings
from pytorch3d.renderer.mesh.rasterizer import MeshRasterizer

import matplotlib.pyplot as plt
import glob
import igl

def create_grid_points_from_bounds(min_bound, max_bound, res):
    x = np.linspace(min_bound, max_bound, res)
    X, Y, Z = np.meshgrid(x, x, x, indexing='ij')
    X = X.reshape((-1,))
    Y = Y.reshape((-1,))
    Z = Z.reshape((-1,))

    points_list = np.column_stack((X, Y, Z))
    del X, Y, Z, x
    return points_list

def todigits(num, len_num):
    num = str(num)
    count_num = len(num)
    if len_num == 6:
        if count_num == 1:
            return '00000'+num 
        elif count_num == 2:
            return '0000'+num
        elif count_num == 3:
            return '000'+num
        elif count_num == 4:
            return '00'+num
        elif count_num == 5:
            return '0'+num
        else:
            return num

    elif len_num == 5:
        if count_num == 1:
            return '0000'+num 
        elif count_num == 2:
            return '000'+num
        elif count_num == 3:
            return '00'+num
        elif count_num == 4:
            return '0'+num
        else:
            return num

    elif len_num == 4:
        if count_num == 1:
            return '000'+num 
        elif count_num == 2:
            return '00'+num
        elif count_num == 3:
            return '0'+num
        else:
            return num

    else:
        print('wrongggggg')

def tsdf_sampling(fl):
    out_file = out_path+fl.split('/')[-1][:-3]+'npz'
    if os.path.exists(out_file):
        print('File exists. Done.')
        return

    gt_mesh = trimesh.load(fl, process=False)
    rescale = 75.0
    
    ###### compute signed distance ###########
    sur_points = np.array(gt_mesh.vertices)
    num_sur = len(sur_points)
    sur_dis_points = np.repeat(sur_points, 26, axis = 0).reshape((num_sur,26,3))+displacments_cube
    sur_dis_points = sur_dis_points.reshape((num_sur*26,3))
    sign_dis = -1.*rescale*igl.signed_distance(sur_dis_points, gt_mesh.vertices, gt_mesh.faces)[0]

    sign_dis = np.array(sign_dis)
    reset_nega = np.where(sign_dis<-1)[0] # reset signed distance lower than threshold to truncated value
    reset_posi = np.where(sign_dis>1)[0] # reset signed distance larger than threshold to truncated value
    
    sign_dis[reset_nega] = -1.0
    sign_dis[reset_posi] = 1.0 
    sign_dis = sign_dis.reshape((num_sur, 26))
    ###### compute signed distance ###########
    
    # keep the surface vertex order and displace order
    ##### from world to ndc #########
    sur_points = torch.from_numpy(sur_points).float().to(device).unsqueeze(0)
    sur_xy_ndc = cameras_depth.transform_points_ndc(sur_points)[0, :, :2]
    sur_dep = cameras_depth.get_world_to_view_transform().transform_points(sur_points)[0, :, 2:]
    # extract depth of each point
    sur_xy_dep = torch.cat((sur_xy_ndc, sur_dep), dim = 1)
    sur_xy_dep = sur_xy_dep.detach().cpu().numpy()

    sur_dis_points = torch.from_numpy(sur_dis_points).float().to(device).unsqueeze(0)
    sur_dis_xy_ndc = cameras_depth.transform_points_ndc(sur_dis_points)[0, :, :2]
    sur_dis_dep = cameras_depth.get_world_to_view_transform().transform_points(sur_dis_points)[0, :, 2:]
    sur_dis_xy_dep = torch.cat((sur_dis_xy_ndc, sur_dis_dep), dim = 1)
    sur_dis_xy_dep = sur_dis_xy_dep.detach().cpu().numpy()
    sur_dis_xy_dep = sur_dis_xy_dep.reshape((num_sur, 26, 3))
    ##### from world to ndc #########
    np.savez(out_file, surface = sur_xy_dep, surface_displaced = sur_dis_xy_dep, sdf_displaced = sign_dis)
    return 

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='camera depth capture'
    )
    parser.add_argument('--subj', type=str, help='5-digit subject id')
    parser.add_argument('--seq', type=str, help='sequence name')
    parser.add_argument('--CAPE', type=str, default='CAPE')
    parser.add_argument('--reso', type=int, default=512)

    args = parser.parse_args()
    device = 'cuda'
    RESO = int(args.reso)
    SUB = str(args.subj)
    SEQ = str(args.seq)
    is_CAPE = (str(args.CAPE) == 'CAPE')
    #####define camera####
    fx = 0.8
    fy = 0.8
    px = 0.0
    py = 0.0
    ######### camera instrinsec ########
    
    ROOT = './'#todo
    #path to data
    ##########out_path##########
    out_path = ROOT+'tsdf_rescale_res'+str(RESO)+'/'
    if not os.path.exists(out_path):
        os.mkdir(out_path)
        print('creating path ', out_path)

    out_path = out_path+SUB+'/'
    if not os.path.exists(out_path):
        os.mkdir(out_path)
        print('creating path ', out_path)

    out_path = out_path+SEQ+'/'
    if not os.path.exists(out_path):
        os.mkdir(out_path)
        print('creating path ', out_path)
    else:
        print('path exist')
    ##########out_path##########

    ########## camera extrinsec #########
    R_camera = torch.zeros((1,3,3))
    R_camera[0,0,0] = -1.0
    R_camera[0,1,1] = 1.0
    R_camera[0,2,2] = -1.0
    T_camera = torch.tensor([[0.002, -0.481, 2.315]])
    # T = [x_center, -y_center-y_dist, z_center+z_dist] y_dist = 0.2, z_dist = 2.5
    print('trans', T_camera)
    cameras_depth = OrthographicCameras(focal_length = ((fx, fy),), principal_point = ((px, py),), R = R_camera, T=T_camera, device=device)
    #####define camera####
    #here we consider only fx == fy
    assert fx==fy, "focal not same for x and y axis"
    displacment = (2.0/fx)/RESO
    displacments_cube = create_grid_points_from_bounds(-1, 1, 3)
    displacments_cube = np.delete(displacments_cube, (13), axis = 0)
    displacments_cube *= displacment
    #displacements of the center query point, 26 points    

    paths = glob.glob(ROOT +SUB+'/'+SEQ+'/*obj')#todo
    #path to ground truth

    for f_p in paths:
        tsdf_sampling(f_p)
    print(out_path)

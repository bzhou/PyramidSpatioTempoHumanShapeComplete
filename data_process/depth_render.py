#=====================================================================================================
# Pyramidal Signed Distance Learning for Spatio-Temporal Human Shape Completion
# Copyright Inria
# Year 2022
# This file is released under the agreement of License.pdf from the git repository.
##
# This repository has code derived from:
## (1) 2020 Julian Chibane, Max-Planck-Gesellschaft (License: https://github.com/jchibane/if-net)
## (2) 2019 Lars Mescheder, Michael Oechsle, Michael Niemeyer, Andreas Geiger, 
## Sebastian Nowozin (MIT License: https://github.com/autonomousvision/occupancy_networks)
## (3) U-Net MICCAI 2015 ( GNU General Public License v3.0: https://github.com/milesial/Pytorch-UNet)
#=====================================================================================================


import torch
from pytorch3d.io import load_obj
from pytorch3d.structures import Meshes
from pytorch3d.renderer import (
    look_at_view_transform,
    FoVPerspectiveCameras,
    PerspectiveCameras, 
    OrthographicCameras,
    PointLights, 
    DirectionalLights, 
    Materials, 
    RasterizationSettings, 
    MeshRenderer, 
    MeshRasterizer,  
    SoftPhongShader,
    TexturesUV,
    TexturesVertex
)
import numpy as np 
import matplotlib.pyplot as plt
import torch.nn as nn 
import trimesh
import argparse
import os
from pytorch3d.renderer.mesh.rasterizer import RasterizationSettings
from pytorch3d.renderer.mesh.rasterizer import MeshRasterizer

import matplotlib.pyplot as plt
import glob

def todigits(num, len_num):
    num = str(num)
    count_num = len(num)
    if len_num == 6:
        if count_num == 1:
            return '00000'+num 
        elif count_num == 2:
            return '0000'+num
        elif count_num == 3:
            return '000'+num
        elif count_num == 4:
            return '00'+num
        elif count_num == 5:
            return '0'+num
        else:
            return num

    elif len_num == 5:
        if count_num == 1:
            return '0000'+num 
        elif count_num == 2:
            return '000'+num
        elif count_num == 3:
            return '00'+num
        elif count_num == 4:
            return '0'+num
        else:
            return num

    elif len_num == 4:
        if count_num == 1:
            return '000'+num 
        elif count_num == 2:
            return '00'+num
        elif count_num == 3:
            return '0'+num
        else:
            return num

    else:
        print('wrongggggg')

class DepthRendererfromMesh(nn.Module):
    def __init__(self, rasterizer):
        super().__init__()
        self.rasterizer = rasterizer
        

    def forward(self, meshes_path):
        verts, faces, aux = load_obj(meshes_path)

        meshes_world = Meshes(
            verts = [verts.to(device)],
            faces = [faces.verts_idx.to(device)]
        )
        fragments = self.rasterizer(meshes_world)
        return fragments.zbuf.detach().cpu().numpy()

def Depthgen(fl):
    out_file = out_path+fl.split('/')[-1][:-3]+'npz'
    if os.path.exists(out_file):
        print('File exists. Done.')
        return
    
    depth = depthRender(fl)
    depth = depth[0,:,:,0]
    depth_mask = np.ma.masked_array(depth, depth < 0)
    depth_min = depth_mask.min()
    assert depth_min>1, 'too close to camera'
    depth_max = depth_mask.max()
    cent_v = (depth_max+depth_min)/2.0 
    depth_ind = np.where(depth >= 0)
    #normalize depth, excepth the =-1 position
    mask_ind = np.where(depth < 0)
    depth_cent = depth.copy()
    depth_cent[mask_ind[0], mask_ind[1]] = -4.0
    depth_cent[depth_ind[0], depth_ind[1]] -= cent_v

    np.savez(out_file, depth_img = depth, depth_cent_img = depth_cent, cent_v = cent_v)
    return 

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='camera depth capture'
    )
    parser.add_argument('--subj', type=str, help='5-digit subject id')
    parser.add_argument('--seq', type=str, help='sequence name, e.g. short????')
    parser.add_argument('--CAPE', type=str, default='CAPE')
    parser.add_argument('--reso', type=int, default=512)

    args = parser.parse_args()
    device = 'cuda'
    RESO = int(args.reso)
    SUB = str(args.subj)
    SEQ = str(args.seq)
    is_CAPE = (str(args.CAPE) == 'CAPE')

    ##########todo out_path##########
    ROOT = './'#todo
    #path to data folder

    out_path = ROOT+'/depth_res'+str(RESO)+'/'
    if not os.path.exists(out_path):
        os.mkdir(out_path)
        print('creating path ', out_path)

    out_path = out_path+SUB+'/'
    if not os.path.exists(out_path):
        os.mkdir(out_path)
        print('creating path ', out_path)

    out_path = out_path+SEQ+'/'
    if not os.path.exists(out_path):
        os.mkdir(out_path)
        print('creating path ', out_path)
    else:
        print('path exist')
    ##########out_path##########  

    #####define camera####
    fx = 0.8
    fy = 0.8
    px = 0.0
    py = 0.0
    ######### camera instrinsec ########  


    ########## camera extrinsec #########
    R_camera = torch.zeros((1,3,3))
    R_camera[0,0,0] = -1.0
    R_camera[0,1,1] = 1.0
    R_camera[0,2,2] = -1.0
    T_camera = torch.tensor([[0.002, -0.481, 2.315]])
    # T = [x_center, -y_center-y_dist, z_center+z_dist] y_dist = 0.2, z_dist = 2.5
    print('trans', T_camera)
    cameras_depth = OrthographicCameras(focal_length = ((fx, fy),), principal_point = ((px, py),), R = R_camera, T=T_camera, device=device)
    #####define camera####

    ######### render depth #########

    raster_settings = RasterizationSettings(
        image_size=RESO, 
        blur_radius=0.0, 
        faces_per_pixel=1, 
    )

    rasterizer = MeshRasterizer(
        cameras=cameras_depth, 
        raster_settings=raster_settings
    )

    depthRender = DepthRendererfromMesh(rasterizer = rasterizer)
    

    paths = glob.glob(ROOT +'partial_data/'+SUB+'/'+SEQ+'/*obj')
    #path to partial data

    for f_p in paths:
        Depthgen(f_p)
    print(out_path)

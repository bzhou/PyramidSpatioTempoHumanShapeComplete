#=====================================================================================================
# Pyramidal Signed Distance Learning for Spatio-Temporal Human Shape Completion
# Copyright Inria
# Year 2022
# This file is released under the agreement of License.pdf from the git repository.
##
# This repository has code derived from:
## (1) 2020 Julian Chibane, Max-Planck-Gesellschaft (License: https://github.com/jchibane/if-net)
## (2) 2019 Lars Mescheder, Michael Oechsle, Michael Niemeyer, Andreas Geiger, 
## Sebastian Nowozin (MIT License: https://github.com/autonomousvision/occupancy_networks)
## (3) U-Net MICCAI 2015 ( GNU General Public License v3.0: https://github.com/milesial/Pytorch-UNet)
#=====================================================================================================

import torch
import torch.nn as nn
import torch.nn.functional as F

bb_max = 0.6    # boundary 

class DoubleConv(nn.Module):
    """(convolution => [BN] => ReLU) * 2"""

    def __init__(self, in_channels, out_channels, mid_channels=None):
        super(DoubleConv, self).__init__()
        if not mid_channels:
            mid_channels = out_channels
        self.double_conv = nn.Sequential(
            nn.Conv2d(in_channels, mid_channels, kernel_size=3, padding=1),
            nn.ReLU(inplace=True),
            nn.Conv2d(mid_channels, out_channels, kernel_size=3, padding=1),
            nn.ReLU(inplace=True)
        )

    def forward(self, x):
        return self.double_conv(x)


class Down(nn.Module):
    """Downscaling with maxpool then double conv"""

    def __init__(self, in_channels, out_channels):
        super().__init__()
        self.maxpool_conv = nn.Sequential(
            nn.MaxPool2d(2),
            DoubleConv(in_channels, out_channels)
        )

    def forward(self, x):
        return self.maxpool_conv(x)

class Up(nn.Module):
    """Upscaling then double conv"""

    def __init__(self, in_channels, out_channels, bilinear=True):
        super().__init__()

        # if bilinear, use the normal convolutions to reduce the number of channels
        if bilinear:
            self.up = nn.Upsample(scale_factor=2, mode='bilinear', align_corners=True)
            self.conv = DoubleConv(in_channels, out_channels, in_channels // 2)
        else:
            self.up = nn.ConvTranspose2d(in_channels , in_channels // 2, kernel_size=2, stride=2)
            self.conv = DoubleConv(in_channels, out_channels)


    def forward(self, x1, x2):
        x1 = self.up(x1)
        diffY = x2.size()[2] - x1.size()[2]
        diffX = x2.size()[3] - x1.size()[3]

        x1 = F.pad(x1, [diffX // 2, diffX - diffX // 2,
                        diffY // 2, diffY - diffY // 2])

        x = torch.cat([x2, x1], dim=1)
        return self.conv(x)

class UGRUNet_hie(nn.Module):
    def __init__(self, n_channels, out_channels, batss, bilinear=True, device = 'cuda'):
        super(UGRUNet_hie, self).__init__()
        self.device = device 
        self.n_channels = n_channels
        self.out_channels = out_channels
        self.bilinear = bilinear
        self.batss = batss

        self.inc = DoubleConv(n_channels, 32)
        self.down1 = Down(32, 64)
        self.down2 = Down(64, 128)
        self.down3 = Down(128, 256)
        factor = 2 if bilinear else 1
        self.down4 = Down(256, 512 // factor)
        self.LayNorm1 = nn.LayerNorm(256)
        
        self.convglob = nn.Conv2d(512//factor,256, kernel_size = 32)
        self.gru = nn.GRU(256, 128, bidirectional = True)
        self.h0 = torch.zeros(2, self.batss, 128).to(self.device)
        self.deconvglob = nn.ConvTranspose2d(256,512//factor, kernel_size = 32)

        self.up1 = Up(512, 256 // factor, bilinear)
        self.up2 = Up(256, 128 // factor, bilinear)
        self.up3 = Up(128, 64 // factor, bilinear)
        self.up4 = Up(64, 64, bilinear)
        self.outc = OutConv(64, out_channels)
        self.outc_down1 = OutConv(64 // factor, out_channels)
        self.outc_down2 = OutConv(128 // factor, out_channels)

    def forward(self, x):
        B, C, dep_n, res1_, res2_ = x.size()
        x11 = self.inc(x[:,0,:,:,:])
        x12 = self.down1(x11)
        x13 = self.down2(x12)
        x14 = self.down3(x13)
        x15 = self.down4(x14)
        x1g = self.convglob(x15).view(B, 1, 256)

        x21 = self.inc(x[:,1,:,:,:])
        x22 = self.down1(x21)
        x23 = self.down2(x22)
        x24 = self.down3(x23)
        x25 = self.down4(x24)
        x2g = self.convglob(x25).view(B, 1, 256)

        x31 = self.inc(x[:,2,:,:,:])
        x32 = self.down1(x31)
        x33 = self.down2(x32)
        x34 = self.down3(x33)
        x35 = self.down4(x34)
        x3g = self.convglob(x35).view(B, 1, 256)

        x41 = self.inc(x[:,3,:,:,:])
        x42 = self.down1(x41)
        x43 = self.down2(x42)
        x44 = self.down3(x43)
        x45 = self.down4(x44)
        x4g = self.convglob(x45).view(B, 1, 256)

        x_gru_input = torch.cat([x1g, x2g, x3g, x4g], 1)
        # B, 4, 256
        x_gru_input = x_gru_input.transpose(0,1)
        # 4 seq, B, 256
        x_gru, h_gru = self.gru(x_gru_input, self.h0)
        # 4 seq, B, 256
        x_gru = x_gru.transpose(0,1)
        # B, 4, 256
        x_gru = F.relu(x_gru)

        x15 = x15+self.deconvglob(x_gru[:,0,:].view(B,256,1,1))
        x25 = x25+self.deconvglob(x_gru[:,1,:].view(B,256,1,1))
        x35 = x35+self.deconvglob(x_gru[:,2,:].view(B,256,1,1))
        x45 = x45+self.deconvglob(x_gru[:,3,:].view(B,256,1,1))

        x1 = self.up1(x15, x14)
        x1_down2 = self.up2(x1, x13)
        x1_down1 = self.up3(x1_down2, x12)
        x1 = self.up4(x1_down1, x11)
        logits1 = self.outc(x1).unsqueeze(2)
        logits1_down1 = self.outc_down1(x1_down1).unsqueeze(2)
        logits1_down2 = self.outc_down2(x1_down2).unsqueeze(2)

        x2 = self.up1(x25, x24)
        x2_down2 = self.up2(x2, x23)
        x2_down1 = self.up3(x2_down2, x22)
        x2 = self.up4(x2_down1, x21)
        logits2 = self.outc(x2).unsqueeze(2)
        logits2_down1 = self.outc_down1(x2_down1).unsqueeze(2)
        logits2_down2 = self.outc_down2(x2_down2).unsqueeze(2)

        x3 = self.up1(x35, x34)
        x3_down2 = self.up2(x3, x33)
        x3_down1 = self.up3(x3_down2, x32)
        x3 = self.up4(x3_down1, x31)
        logits3 = self.outc(x3).unsqueeze(2)
        logits3_down1 = self.outc_down1(x3_down1).unsqueeze(2)
        logits3_down2 = self.outc_down2(x3_down2).unsqueeze(2)

        x4 = self.up1(x45, x44)
        x4_down2 = self.up2(x4, x43)
        x4_down1 = self.up3(x4_down2, x42)
        x4 = self.up4(x4_down1, x41)
        logits4 = self.outc(x4).unsqueeze(2)
        logits4_down1 = self.outc_down1(x4_down1).unsqueeze(2)
        logits4_down2 = self.outc_down2(x4_down2).unsqueeze(2)

        logits_origin = torch.cat((logits1, logits2, logits3, logits4), 2)
        logits_down1 = torch.cat((logits1_down1, logits2_down1, logits3_down1, logits4_down1), 2)
        logits_down2 = torch.cat((logits1_down2, logits2_down2, logits3_down2, logits4_down2), 2)
        return [logits_origin, logits_down1, logits_down2]


class OutConv(nn.Module):
    def __init__(self, in_channels, out_channels):
        super(OutConv, self).__init__()
        self.conv = nn.Conv2d(in_channels, out_channels, kernel_size=1)

    def forward(self, x):
        return self.conv(x)

upsample_2 = nn.Upsample(scale_factor=2, mode='bilinear', align_corners=True)

class CNN_trans(nn.Module):
    def __init__(self, n_channels, out_channels):
        super(CNN_trans, self).__init__()
        self.n_channels = n_channels
        self.out_channels = out_channels

        self.conv1 = nn.Conv3d(self.n_channels, self.out_channels, kernel_size=3, padding=1)
        self.conv2 = nn.Conv3d(self.out_channels, self.out_channels, kernel_size=3, padding=1)
        self.conv3 = nn.Conv3d(self.out_channels, self.out_channels, kernel_size=3, padding=1)

    def forward(self, x, query_pts, feat_init = 0, flag = False, query_flag = False):
        B, dep_num, N, _ = query_pts.size()
        
        if flag:
            feat_init1 = upsample_2(feat_init[:,:,0,:,:]).unsqueeze(2)
            feat_init2 = upsample_2(feat_init[:,:,1,:,:]).unsqueeze(2)
            feat_init3 = upsample_2(feat_init[:,:,2,:,:]).unsqueeze(2)
            feat_init4 = upsample_2(feat_init[:,:,3,:,:]).unsqueeze(2)
            feat_init_up = torch.cat((feat_init1, feat_init2, feat_init3, feat_init4), 2)
            x = torch.cat((x, feat_init_up), 1)
            
        else:
            feat_init_up = 0

        x = F.relu(self.conv1(x))
        x = F.relu(self.conv2(x))
        x = self.conv3(x)+feat_init_up

        if query_flag:
            feats_out = []
            for i_dep in range(dep_num):
                query_pts_to = query_pts[:,i_dep,:,:].unsqueeze(1)
                #(B,1,N,3)
                
                grid_query = query_pts_to[:,:,:,[1,0]]
                grid_query /= (1.*bb_max) # rescale to -1,1 wrt to depth

                feats_1 = F.grid_sample(x[:,:,i_dep,:,:], grid_query, mode = 'bilinear', align_corners=True).view(B, self.out_channels, N).transpose(1,2)
                #(B, N, C)
                feats_out.append(feats_1)

            return feats_out, x

        else:
            return x 

class Decode_net(nn.Module):
    def __init__(self, in_channels, out_channels):
        super(Decode_net, self).__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.spa1 = nn.Linear(in_channels, in_channels//2)
        self.spa2 = nn.Linear(in_channels//2, in_channels//2)
        self.spa3 = nn.Linear(in_channels//2, out_channels)

    def forward(self, x):
        outy = F.relu(self.spa1(x))
        outy = F.relu(self.spa2(outy))
        outy = self.spa3(outy)
        return torch.tanh(outy)

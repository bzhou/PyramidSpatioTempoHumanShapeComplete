# PyramidSpatioTempoHumanShapeComplete

This repository contains the code of the paper [**Pyramidal Signed Distance Learing for Spatio-Temporal Human Shape Completion**](https://openaccess.thecvf.com/content/ACCV2022/papers/Zhou_Pyramidal_Signed_Distance_Learning_for_Spatio-Temporal_Human_Shape_Completion_ACCV_2022_paper.pdf) ACCV2022.

# Requirements

- python3
- pytorch>1.2.0
- trimesh
- scikit-image
- pykdtree

# Demo

## Pretrained model:

download the pretrained network weights from

put them into folder weights.
```
weights
│   encode.pt
│   ...       
```
Put front-view partial mesh or point cloud in folder partial_data or partial_pc.
Note that we define front-view as z+ direction. The pre-trained model doesn't work for side-view or top-view. We prepare 2 partial sequences to test.
```
partial_data
└───CAPE
    └───sequence
```
run following code and check the results in folder result:
```
python complete.py --dataset CAPE --seq shortshort_tilt_twist_left --start 000400
python complete.py --dataset DFAUST --seq punching --start 00024
```
Other than partial mesh, you can test our method for 'dense'(>100000) partial point cloud:
```
python complete_pc.py --dataset CAPE --seq shortshort_tilt_twist_left --start 000400
python complete_pc.py --dataset DFAUST --seq punching --start 00024
```
# Data pre-processing

## New requirements

- pytorch3d = 0.6.1
- igl

download data from two datasets:

**CAPE** <https://cape.is.tue.mpg.de>

**DFAUST** <https://dfaust.is.tue.mpg.de>

We need both raw scan data and SMPL-fitted ground truth.

## Depth rendering

run following code in folder data_process:
```
python data_process/depth_render.py --subj ????? --seq short???? --reso 512
```

## TSDF pre-computation

```
python data_process/tsdf_reso_3d.py --subj ????? --seq short???? --reso 512
python data_process/tsdf_reso_3d.py --subj ????? --seq short???? --reso 256
python data_process/tsdf_reso_3d.py --subj ????? --seq short???? --reso 128
python data_process/tsdf_more_uniform_3d.py --subj ????? --seq short???? --interv 4
python data_process/tsdf_more_uniform_3d.py --subj ????? --seq short???? --interv 10
```

**Note**: camera parameters have been pre-defined for aforementioned datasets, but such parameters might be modified for other data for the reason of visibility.

If you find our work useful, please cite:
```
@inproceedings{pyramidcomplete_accv2022,
	title = {Pyramidal Signed Distance Learning for Spatio-Temporal Human Shape Completion},
	author = {Zhou, Boyao and Franco, Jean-Sebastien and deLaGorce, Martin and Boyer, Edmond},
	booktitle = {Proceedings of the Asian Conference on Computer Vision},
	year = {2022}
}

@inproceedings{STIF_3dv2021,
	title = {Spatio-Temporal Human Shape Completion With Implicit Function Networks},
	author = {Zhou, Boyao and Franco, Jean-Sebastien and Bogo, Federica and Boyer, Edmond},
	booktitle = {Proceedings of the International Conference on 3D Vision},
	year = {2021}
}
```

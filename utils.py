#=====================================================================================================
# Pyramidal Signed Distance Learning for Spatio-Temporal Human Shape Completion
# Copyright Inria
# Year 2022
# This file is released under the agreement of License.pdf from the git repository.
##
# This repository has code derived from:
## (1) 2020 Julian Chibane, Max-Planck-Gesellschaft (License: https://github.com/jchibane/if-net)
## (2) 2019 Lars Mescheder, Michael Oechsle, Michael Niemeyer, Andreas Geiger, 
## Sebastian Nowozin (MIT License: https://github.com/autonomousvision/occupancy_networks)
## (3) U-Net MICCAI 2015 ( GNU General Public License v3.0: https://github.com/milesial/Pytorch-UNet)
#=====================================================================================================

import numpy as np
import torch 
import torch.nn.functional as F
import trimesh 


def query_feat(x, query_pts):
    bb_min = -0.6
    bb_max = 0.6
    B, N, _ = query_pts.size()
    bb_, Cha_s, r_, r_ = x.size()
    query_pts = query_pts.unsqueeze(1)
    #(B,1,N,3)
    
    grid_query = query_pts[:,:,:,[1,0]]
    grid_query /= (1.*bb_max) # rescale to -1,1 wrt to depth

    feats = F.grid_sample(x, grid_query, mode = 'bilinear', align_corners=True).view(B, Cha_s, N).transpose(1,2)
    #(B, N, C)
    return feats

def todigits(num, len_num):
    num = str(num)
    count_num = len(num)
    if len_num == 6:
        if count_num == 1:
            return '00000'+num 
        elif count_num == 2:
            return '0000'+num
        elif count_num == 3:
            return '000'+num
        elif count_num == 4:
            return '00'+num
        elif count_num == 5:
            return '0'+num
        else:
            return num

    elif len_num == 5:
        if count_num == 1:
            return '0000'+num 
        elif count_num == 2:
            return '000'+num
        elif count_num == 3:
            return '00'+num
        elif count_num == 4:
            return '0'+num
        else:
            return num

    elif len_num == 4:
        if count_num == 1:
            return '000'+num 
        elif count_num == 2:
            return '00'+num
        elif count_num == 3:
            return '0'+num
        else:
            return num

    else:
        print('not implement')

def grid_points_from_bounds(min_bound, max_bound, res):
    x = np.linspace(min_bound, max_bound, res)
    X, Y, Z = np.meshgrid(x, x, x, indexing='ij')
    X = X.reshape((-1,))
    Y = Y.reshape((-1,))
    Z = Z.reshape((-1,))

    points_list = np.column_stack((X, Y, Z))
    del X, Y, Z, x
    return points_list

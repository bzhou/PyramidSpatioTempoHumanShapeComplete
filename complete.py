#=====================================================================================================
# Pyramidal Signed Distance Learning for Spatio-Temporal Human Shape Completion
# Copyright Inria
#  Year 2022
# This file is released under the agreement of License.pdf from the git repository.
##
# This repository has code derived from:
## (1) 2020 Julian Chibane, Max-Planck-Gesellschaft (License: https://github.com/jchibane/if-net)
## (2) 2019 Lars Mescheder, Michael Oechsle, Michael Niemeyer, Andreas Geiger, 
## Sebastian Nowozin (MIT License: https://github.com/autonomousvision/occupancy_networks)
## (3) U-Net MICCAI 2015 ( GNU General Public License v3.0: https://github.com/milesial/Pytorch-UNet)
#=====================================================================================================

import torch 
import trimesh
import torch.nn.functional as F
from networks.ugrunet import *
import numpy as np
import matplotlib.pyplot as plt
from scipy import ndimage
import glob
from skimage import measure
from pykdtree.kdtree import KDTree
from utils import grid_points_from_bounds, query_feat, todigits 
import os 
import argparse

parser = argparse.ArgumentParser(description='human shape completion')
parser.add_argument('--dataset', type=str, help = 'option in [CAPE, DFAUST]', default='CAPE')
parser.add_argument('--seq', type=str, help = 'sequence name', default='shortshort_tilt_twist_left')
parser.add_argument('--start', type=str, help = 'start frame, 6-digit for CAPE, 5-digit for DFAUST', default='000400')
parser.add_argument('--plot', type=str, help = 'plot input depth', default='yes')

torch.set_grad_enabled(False)
device = 'cuda'
RESO = 512      # input depth resolution
bb_min = -0.6
bb_max = 0.6    # boundary 
TRESH = 0.0     # level-set
RESO_out = 256  # output grid resolution

'''todo modify your path and data folder'''
ROOT = './'

args = parser.parse_args()
data_set = args.dataset
SEQ = args.seq 
START_FRAME = args.start
plot_depth = (args.plot == 'yes')
if data_set in ['CAPE','DFAUST']:
    files = [ROOT+data_set+'/'+SEQ+'/'+SEQ+'.'+START_FRAME+'.obj']
'''
elif data_set == 'RenderPeople':
    files = [ROOT+data_set+'/'+SEQ+'/'+START_FRAME+'.obj']
'''
else:
    print('not implement')

out_path = ROOT+'result/'

if data_set in ['DFAUST', 'CAPE']:
    INTERV_VAD = 4 #todo, 60fps
'''
elif data_set in ['RenderPeople']:
    INTERV_VAD = 2 #todo, 30fps
'''
else:
    print('not valid, todo')




BATCH_S = 1

#########network loading###########
bottle_neck_dim = 128
encode = UGRUNet_hie(1, bottle_neck_dim, BATCH_S, bilinear=False).to(device)
encode.load_state_dict(torch.load(ROOT+'weights/encode.pt'))
encode.eval()

conv_trans_init = CNN_trans(bottle_neck_dim, bottle_neck_dim).to(device)
conv_trans_init.load_state_dict(torch.load(ROOT+'weights/conv3d_ini.pt'))
conv_trans_init.eval()

conv_trans = CNN_trans(bottle_neck_dim*2, bottle_neck_dim).to(device)
conv_trans.load_state_dict(torch.load(ROOT+'weights/conv3d.pt'))
conv_trans.eval()

decode = Decode_net(bottle_neck_dim+1,1).to(device)
decode.load_state_dict(torch.load(ROOT+'weights/decode.pt'))
decode.eval()
#########network loading###########




########## prepare input depth, only for completion case
grid_points_input = grid_points_from_bounds(bb_min, bb_max, RESO)
kdtree_inp = KDTree(grid_points_input)

def load_data(f_path, interv, ROOT):
    '''
    args:
        f_path: seqence/start_frame
        interv: interval frame 
            4 for CAPE/DFAUST 60fps
            2 for RenderPeople 30fps
        ROOT: 
            partial_data/ partial mesh
            partial_pc/ partial dense point cloud
            the model is pre-trained with front-view(z+) partial data
    outs:
        depth_seq: (B = 1, N = 4, 1, reso = 512, 512)
        loc_scale: to rescale and relocalte the output mesh
        output_path   
    '''
    fl_split = f_path.split('/')
    fl = fl_split[-1]
    fl_parts = fl.split('.')
    '''
    if data_set == 'RenderPeople':
        fl_nex1 = str(int(fl_parts[0])+interv)+'.'+fl_parts[1]
        fl_nex2 = str(int(fl_parts[0])+interv*2)+'.'+fl_parts[1]
        fl_end = str(int(fl_parts[0])+interv*3)+'.'+fl_parts[1]
    '''
    if data_set in ['CAPE','DFAUST']:
        fl_nex1 = fl_parts[0]+'.'+todigits(int(fl_parts[1])+interv, len(fl_parts[1]))+'.'+fl_parts[2]
        fl_nex2 = fl_parts[0]+'.'+todigits(int(fl_parts[1])+interv*2, len(fl_parts[1]))+'.'+fl_parts[2]
        fl_end = fl_parts[0]+'.'+todigits(int(fl_parts[1])+interv*3, len(fl_parts[1]))+'.'+fl_parts[2]
    else:
        print('not implement')

    loc_scale = []
    depth_inp = []
    bb_max = 0.6
    inp_num = 300000
    RESO = 512
    count_in = 0
    for fpl in [fl, fl_nex1, fl_nex2, fl_end]:
        part_path = ROOT+'/'+fl_split[-2]+'/'+fpl
        count_in += 1

        part_mesh = trimesh.load(part_path, process = False)

        bounds = part_mesh.bounds
        loc = (bounds[0] + bounds[1]) / 2
        scale = (bounds[1] - bounds[0]).max()
        loc = np.asarray(loc)
        scale = float(scale)
        loc_scale.append((loc, scale))   
        part_mesh.apply_translation(-loc)
        part_mesh.apply_scale(1/scale)

        if data_set not in ['DFAUST', 'CAPE']:
            verts_HR = trimesh.sample.sample_surface(part_mesh, inp_num)[0]
        else:
            verts_HR = part_mesh.vertices

        occupancies = np.zeros(len(grid_points_input), dtype=np.int8)

        _, idx = kdtree_inp.query(verts_HR)
        occupancies[idx] = 1        

        occ = occupancies.reshape((RESO, RESO, RESO))
        pic = np.zeros((RESO,RESO))

        for i in range(RESO):
            for j in range(RESO):
                line = occ[i][j]
                line_in = np.where(line == 1)[0]
                if np.any(line_in):
                    idx_select = max(line_in)
                    pt_select = grid_points_input[i*RESO*RESO+j*RESO+idx_select]
                    pic[i][j] = np.absolute(bb_max-pt_select[2]) 
        
        depth_inp.append(pic)
        if plot_depth:
            in_depth = ndimage.rotate(pic, 90)
            plt.matshow(in_depth, cmap='gray')
            plt.title('input depth frame '+str(count_in))
            plt.axis('off')

    if plot_depth:
        plt.show()
    
    depth_seq = np.array(depth_inp).reshape(1,4,1,RESO,RESO)
    return depth_seq, loc_scale, [fl, fl_nex1, fl_nex2, fl_end]

######output path
if not os.path.exists(out_path):
    os.mkdir(out_path)
    print('creating path ', out_path)
out_path = out_path+str(data_set)+'/'
if not os.path.exists(out_path):
    os.mkdir(out_path)
    print('creating path ', out_path)
######output path

grid_points = grid_points_from_bounds(bb_min, bb_max, RESO_out) # grid pts to query
print_i = 0

for tar_file in files:

    depp, loc_scale, out_files  = load_data(tar_file, INTERV_VAD, ROOT+'partial_data/'+data_set)
    depp = torch.from_numpy(depp).float().to(device) 
    print('----following is----------', print_i+1,'/', len(files), '--', files[print_i].split('/')[-3], '--', files[print_i].split('/')[-1],'----------------')


    gt_ii = 0


    tar_path = out_path + tar_file.split('/')[-2]+'/'
    qrpt_nex1 = torch.rand(1,10,3).float().to(device) #not necessary
    if not os.path.exists(tar_path):
        os.mkdir(tar_path)
        print('creating path ', tar_path)
    
    feat_seq_512, feat_seq_256, feat_seq_128 = encode(depp)
    for ti in range(4):
        to_obj = tar_path+out_files[gt_ii]
        trans_init = conv_trans_init(feat_seq_128, qrpt_nex1.unsqueeze(1))
        trans_256 = conv_trans(feat_seq_256, qrpt_nex1.unsqueeze(1), trans_init, True)
        trans_512 = conv_trans(feat_seq_512, qrpt_nex1.unsqueeze(1), trans_256, True)
        res_vox = np.zeros((RESO_out)**3)
        
        for bi in range(RESO_out):
            qrbi = torch.from_numpy(grid_points[((RESO_out)**2)*bi:((RESO_out)**2)*(bi+1),:]).float().unsqueeze(0).to(device)
            cnn_feat_512 = query_feat(trans_512[:,:,gt_ii,:,:], qrbi)
            lin_feat_512 = qrbi[:,:,-1].unsqueeze(-1)*128.
            out_batch = decode(torch.cat((cnn_feat_512, lin_feat_512),-1))
            Y_pred = out_batch.detach().cpu().numpy().ravel()
            res_vox[((RESO_out)**2)*bi:((RESO_out)**2)*(bi+1)] = Y_pred.copy()

        vos = (res_vox.reshape((RESO_out,RESO_out,RESO_out)))

        verts, faces, normals, values = measure.marching_cubes(vos, TRESH, gradient_direction='ascent')
        verts = (verts-(RESO_out-1)/2)*(bb_max/0.5)/(RESO_out-1)*loc_scale[gt_ii][1]+loc_scale[gt_ii][0]

        mesh_output = trimesh.Trimesh(vertices = verts, faces = faces)
        mesh_output.export(to_obj)

        gt_ii += 1 

    print_i += 1
